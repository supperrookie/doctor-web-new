import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, MenuItem, MessageService } from 'primeng/api';
import { DoctorOrderService } from './doctor-order-service';
import { DateTime } from 'luxon';
import * as _ from 'lodash';

@Component({
    selector: 'app-doctor-order',
    templateUrl: './doctor-order.component.html',
    styleUrls: ['./doctor-order.component.scss'],
    providers: [MessageService],
    
})
export class DoctorOrderComponent implements OnInit {
    parentData:any;
    dataLoaded: boolean = false;
    // dataLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    query: any = '';
    dataSet: any[] = [];
    loading = false;

    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;

    // progress note data
    progressValueS?: string;
    progressValueO?: string;
    progressValueA?: string;
    progressValueP?: string;
    onedayValue?: string;
    continueValue?: string;

    // doctor order data
    doctorOrderData: any;
    listDoctorOrder: any;

    userData : any;

    // is new order
    isNewOrder: boolean = true;

    progressTags = ['progressValue'];
    onedayTags = ['onedayValue'];
    continueTags = ['onedayValue'];

    inputVisible = false;
    inputValue?: string = '';

    patientList: any;
    orderList: any;
    drugList: any;

    hn: any;
    an: any;

    title: any;
    fname: any;
    lname: any;
    gender: any;
    age: any;
    address: any;
    phone: any;

    admit_id: any;
    doctor_order_date: any;
    doctor_order_time: any;
    doctor_order_by: any;
    is_confirm: any;

    subjective: any;
    objective: any;
    assertment: any;
    plan: any;
    note: any;

    order_type_id: any;
    item_type_id: any;
    item_id: any;
    item_name: any;
    medicine_usage_code: any;
    medicine_usage_extra: any;
    quantity: any;

    food_id: any;
    start_date: any;
    start_time: any;
    end_date: any;
    end_time: any;

    is_vital_sign: any;

    progress_note: any;
    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];
    oneDay: any[] = [];
    continue: any[] = [];
    orders: any[] = [];
    ordersOneday: any[] = [];
    continues: any[] = [];

    medicine: any[] = [];
    speedDialitems: MenuItem[] = [];
    progress_note_id: any;

    drugname: any;
    indeterminate = true;

    queryParamsData: any;
    value?: string;
    inputValue1?: string;
    jsonData:any={
        
    }
    

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private messageService: MessageService,

        private doctorOrderService: DoctorOrderService
    ) {
        let _data = sessionStorage.getItem('doctor_order');
        sessionStorage.removeItem('doctor_order');
        console.log(_data);
        if (_data != "undefined" && _data != null) {
            this.doctorOrderData = JSON.parse(_data!);
            this.isNewOrder = false;
        } else {
            this.isNewOrder = true;
        }

        let _user = sessionStorage.getItem('userData');
        if (_user != "undefined" && _user != null) {
            let user = JSON.parse(_user!);
            this.userData = user;
        } else {
            this.userData = null;
        }

        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
        // this.parentData = '88888';

    }

    ngOnInit(): void {
        // console.log('is init doctor order');
        this.getPatient();
        this.buttonSpeedDial();
    }

    ngAferViewInit() {
        // this.getPatient();
    }


    async getPatient() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            //  console.log(this.queryParamsData);
            let admitId = this.queryParamsData;

            const response = await this.doctorOrderService.getPatientInfo(   admitId   );

            const data: any = response.data;

            this.patientList = await data.data;
            //console.log(this.patientList);
            this.hn = this.patientList.hn;
            this.an = this.patientList.an;
            this.title = this.patientList.patient.title;
            this.fname = this.patientList.patient.fname;
            this.lname = this.patientList.patient.lname;
            this.gender = this.patientList.patient.gender;
            this.age = this.patientList.patient.age;
            this.address = this.patientList.address;
            this.phone = this.patientList.phone;

            this.total = data.total || 1;
            this.messageService.add({
                severity: 'success',
                summary: 'กระบวนการสำเร็จ #',
                detail: '.....',
            });

            let docterOrder = await this.doctorOrderService.getDoctorOrderById(this.queryParamsData);
            this.dataLoaded =true;

            if(docterOrder.data.data.length > 0)
            {
                let sortData = _.orderBy(docterOrder.data.data, ["doctor_order_date", "doctor_order_time"],['desc', 'desc']); 
                console.log(sortData);
                this.listDoctorOrder = sortData;
                this.isNewOrder = sortData[0].is_confirm;
                console.log(this.listDoctorOrder);
                console.log(this.isNewOrder);
                var date2 = DateTime.now();
                var date3 = DateTime.now();
                const date5 = date2.toFormat('yyyy-MM-dd');
                const time1 = date3.toFormat('HH:mm:ss');
                let doctor_order = [{
                    "id": sortData[0].id,
                    "admit_id": sortData[0].admit_id,
                    "doctor_order_date":sortData[0].doctor_order_date,
                    "doctor_order_time": sortData[0].doctor_order_time,
                    "doctor_order_by": sortData[0].doctor_order_by,
                    "is_confirm": sortData[0].is_confirm,
                    "is_active": true,
                    "create_date":  sortData[0].doctor_order_date,
                    "create_by": null,
                    "modify_date": date5,
                    "modify_by": this.userData.id || null,
                    "status": "pending",
                    "confirm_by": null,
                    "confirm_date": null,
                    "confirm_time": null
                }]

                console.log(doctor_order);
                this.parentData = {'isNewOrder':this.isNewOrder,'doctor_order':doctor_order};
                this.dataLoaded =true;

              
            }else{
                this.isNewOrder = true;
                console.log(this.isNewOrder);
                this.parentData = {'isNewOrder':true,'doctor_order':[]};
                this.dataLoaded =true;

            }
            
         

        } catch (error: any) {
            console.log(error);            

            this.messageService.add({
                severity: 'dager',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    back() {
        sessionStorage.removeItem('doctor_order');
        this.router.navigate(['/list-patient']);
    }

    buttonSpeedDial(){
        this.speedDialitems = [
          {
            icon: 'pi pi-arrow-left',
            routerLink: ['/list-patient'],
            tooltipOptions: {
              tooltipLabel: 'ย้อนกลับ',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-circle-info',
            command: () => {
              this.navigatePatientInfo()
            },
            tooltipOptions: {
              tooltipLabel: 'Patient Info',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-comment-medical',
            command: () => {
              this.navigateAdmisstionNote()
            },
            tooltipOptions: {
              tooltipLabel: 'Admission Note',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-heart-pulse',
            command: () => {
              this.navigateEkg()
            },
            tooltipOptions: {
              tooltipLabel: 'EKG',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-phone-volume',
            command: () => {
              this.navigateConsult()
            },
            tooltipOptions: {
              tooltipLabel: 'Consult',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-vial-virus',
            command: () => {
              this.navigateLab()
            },
            tooltipOptions: {
              tooltipLabel: 'Lab',
              tooltipPosition: 'bottom'
            },
          }
        ];
      } 

    navigatePatientInfo(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

    }

    navigateAdmisstionNote(){
        let jsonString = JSON.stringify(this.queryParamsData);
        console.log(jsonString);   
        this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });
    
      }
    
      navigateEkg(){
        let jsonString = JSON.stringify(this.queryParamsData);
        console.log(jsonString);   
        this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });
    
      }
    
      navigateConsult(){
        let jsonString = JSON.stringify(this.queryParamsData);
        console.log(jsonString);   
        this.router.navigate(['/consult'], { queryParams: { data: jsonString } });
    
      }
      navigateLab(){
        let jsonString = JSON.stringify(this.queryParamsData);
        console.log(jsonString);   
        this.router.navigate(['/lab'], { queryParams: { data: jsonString } });
    
      }
}

/////////doctor_order model////////////////////////
// {
    // "id": "90cd66b1-e62c-4934-b8fc-c49f8c8172de",
    // "admit_id": "bd50202e-2f54-4c6c-99d6-8c1867168d8d",
    // "doctor_order_date": "2024-03-01T17:00:00.000Z",
    // "doctor_order_time": "21:42:39-07",
    // "doctor_order_by": "89edf6c5-ef91-4037-b2dd-8488a49016dd",
    // "is_confirm": false,
    // "is_active": true,
    // "create_date": "2024-03-02T14:42:39.100Z",
    // "create_by": null,
    // "modify_date": "2024-03-02T14:42:39.100Z",
    // "modify_by": null,
    // "status": "pending",
    // "confirm_by": null,
    // "confirm_date": null,
    // "confirm_time": null
// }