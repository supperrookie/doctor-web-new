import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';
import { DataView } from 'primeng/dataview';

@Injectable({
  providedIn: 'root'
})
export class AdmisstionNoteService {

    pathPrefixLookup: any = `api-lookup/lookup`
    pathPrefixDoctor: any = `api-doctor/doctor`
    pathPrefixAuth: any = `api-auth/auth`
  
    private axiosInstance = axios.create({
      baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
    })
  
    private axiosLookup = axios.create({
      baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
    })
  
    constructor() {
      
      this.axiosInstance.interceptors.request.use(config => {
        const token = sessionStorage.getItem('token')
        if (token) {
          config.headers['Authorization'] = `Bearer ${token}`
        }
        return config
      });
  
      this.axiosInstance.interceptors.response.use(response => {
        return response
      }, error => {
        return Promise.reject(error)
      })
  
  
  
  
      this.axiosLookup.interceptors.request.use(config => {
        const token = sessionStorage.getItem('token')
        if (token) {
          config.headers['Authorization'] = `Bearer ${token}`
        }
        return config
      });
  
      this.axiosLookup.interceptors.response.use(response => {
        return response
      }, error => {
        return Promise.reject(error)
      })
  
  
  
    }
  
    async getActive(wardId: any, query: any, limit: any, offset: any) {
      const url = `/admit?ward_id=${wardId}&query=${query}&limit=${limit}&offset=${offset}`
      return await this.axiosInstance.get(url)
    }
  
    async getWaiting(limit: any, offset: any) {
      const url = `/ward?limit=${limit}&offset=${offset}`
      return await this.axiosInstance.get(url)
    }
  
    async getPatientAdmit(wardId: any) {
      //  - /doctor/ward/:ward_id/patient-admit   #GET  
      // /doctor/admit/${wardID}/patient-admit
      const url = `/admit/${wardId}/patient-ward`
      return await this.axiosInstance.get(url)
    }
  
  
    async getPatientInfo(admitId: any) {
      console.log(admitId);
  
      const url = `/admit/${admitId}/patient-info`
  
      return await this.axiosInstance.get(url)
    }
  
    async saveAdmissionNote(data: any) {
    const url = `/admission-note`
  
     return await this.axiosInstance.post(url,data)
    //  const url = `/admission-note/${admitId}`
     // return await this.axiosInstance.get(url)
    }
    async getAdmissionNote(admitId: any) {
      //const url = `/admission-note`
      const url = `/admission-note/${admitId}`
    //  return await this.axiosInstance.post(url,data)
      return await this.axiosInstance.get(url)
    }
  
  
    async saveDoctorOrder(data: object) {
      return await this.axiosInstance.post('/doctor-order', data)
    }
  
    async getDoctorOrderById(admitId: any) {
      console.log(admitId);
  
      const url = `/doctor-order/${admitId}/admit`
  
      return await this.axiosInstance.get(url)
    }
  
  
    async getLookupMedicine() {
      console.log('getLookupMedicine');
      const url = `/medicine`
      return await this.axiosLookup.get(url)
    }
    async saveProgressNote(data: object) {
      const url = `/progress-note`
      return await this.axiosInstance.post(url, data)
    }
  
    async getItems(itemTypeID:any) {
      console.log('getItems');
      // /lookup/item/item-type/:itemTypeID
      const url = `/item/item-type/${itemTypeID}`
      return await this.axiosLookup.get(url)
    }
  
    



}
